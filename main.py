##
## This file is part of the Space Launcher project.
## https://gitlab.com/ballarat-hackerspace/projects/space-launcher
##
## Author(s):
##  - Sen <me@obsoletenerd.com>
##
## Space Launcher is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Space Launcher is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Space Launcher.  If not, see <http://www.gnu.org/licenses/>.
##

# From MicroPython:
import esp32
from machine import Pin
from machine import deepsleep
from time import sleep
import micropython
import network

# From This Project:
from umqttsimple import MQTTClient
import secrets

# LED pin (through 220ohm resistor)
led = Pin(27, Pin.OUT)

# Button pin (to 5V, with 10k resistor between Pin and GND)
push_button = Pin(13, Pin.IN)

# WiFi and MQTT details should be stored in secrets.py
ssid = secrets.SSID
password = secrets.PASS
mqtt_server = secrets.MQTT_SERVER
mqtt_user = secrets.MQTT_USER
mqtt_pass = secrets.MQTT_PASS

# MQTT details
client_id = "SpaceLauncher"
topic_sub = b"streams/space/button"
topic_pub = b"streams/space/button"

# Deep Sleep Settings
esp32.wake_on_ext0(pin=push_button, level=esp32.WAKEUP_ANY_HIGH)

# Let's Go!
print("Im awake. Going to do my MQTT stuff now.")

station = network.WLAN(network.STA_IF)
station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
    pass

print("Connection successful")
print(station.ifconfig())


def sub_cb():
    pass


def connect_and_subscribe():
    global client_id, mqtt_server, topic_sub, mqtt_user, mqtt_pass
    client = MQTTClient(client_id, mqtt_server, 1883, mqtt_user, mqtt_pass)
    client.set_callback(sub_cb)
    client.connect()
    client.subscribe(topic_sub)
    print(
        "Connected to %s MQTT broker, subscribed to %s topic" % (mqtt_server, topic_sub)
    )
    return client


def restart_and_reconnect():
    print("Failed to connect to MQTT broker. Reconnecting...")
    time.sleep(10)
    machine.reset()


try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()

msg = b"Pressed"
client.publish(topic_pub, msg)
led.value(1)  # LED ON
sleep(10)
led.value(0)  # LED OFF

print("Going to sleep now.")
deepsleep()
