# Space Launcher

![Space Launcher Button](https://gitlab.com/ballarat-hackerspace/projects/space-launcher/-/raw/main/Space%20Launcher%20-%201.jpeg)

Space Launcher is a physical button we press to open/close the Ballarat Hackerspace. Pressing the button sends a notification to our Streams/MQTT system, which then allows actions such as:

- Sending chat messages to our members notifying them the space is now open
- Turning machines/lights/etc on/off
- Whatever else members can come up with

This first version of the project consists of an ESP32 dev board with MicroPython installed, a small LiPo battery pack, an Adafruit LiPo management board, a comically large arcade button, and a laser cut wooden box.

This project could easily be adapted to anywhere that you want a button sending data to an MQTT server.

## Instructions

- Copy `main.py` and `umqttsimple.py` to your MicroPython-equipped ESP32 (using [Thonny](https://thonny.org) or similar)
- Create a file called `secrets.py` that has the following constants and copy it to the ESP32:

```
SSID = "wifi_ssid"
PASS = "wifi_pass"
MQTT_SERVER = "IP.or.domain.tld"
MQTT_USER = "mqtt_user"
MQTT_PASS = "mqtt_pass"
```

- Connect Pin 27 to the LED inside the arcade button via a 220ohm resistor, and the other side of the LED to GND.
- Connect Pin 13 to one side of the button switch, and the other side of the switch to GND.

There is a Lightburn file for the laser cut wooden box design, and a ready-to-cut RD file to re-cut it directly with 3mm MDF.

![Space Launcher Button](https://gitlab.com/ballarat-hackerspace/projects/space-launcher/-/raw/main/Space%20Launcher%20-%202.jpeg)
